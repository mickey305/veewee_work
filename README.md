# [my note]
# [create vagrant box file from vm with [veewee][link1] command]
e.g. CentOS v6.4 64bit minimal

# [requirements]
* bundle (e.g. don't exist - `gem install bundler`)

# [usage]


### how to - 1 ###
---

* terminal commands - 1

```
#### make a work directory
mkdir veewee_work
cd veewee_work/
bundle init

vi Gemfile
```

* gemfile

```ruby
#### edit : push 'i' or 'a'
source "https://rubygems.org"

# gem "rails"
gem "veewee"
gem "vagrant"
#### store : push escape-key + ':wq' + enter-key
```

* terminal commands - 2

```
bundle install --path vendor/bundle --binstubs
# bundle install --path vendor/bundle --binstubs vendor/bundle_bin
bundle exec bin/veewee vbox define CentOS-6.4-x86_64-minimal CentOS-6.4-x86_64-minimal

#### edit some definitions

bundle exec bin/veewee vbox build CentOS-6.4-x86_64-minimal
bundle exec bin/veewee vbox export CentOS-6.4-x86_64-minimal
# bundle exec vendor/bundle_bin/veewee vbox build CentOS-6.4-x86_64-minimal
# bundle exec vendor/bundle_bin/veewee vbox export CentOS-6.4-x86_64-minimal
```

### how to - 2 ###
---

* terminal commands

```
git clone git@bitbucket.org:mickey305/veewee_work.git
cd veewee_work/

bundle install --path vendor/bundle --binstubs
# bundle install --path vendor/bundle --binstubs vendor/bundle_bin

#### edit some definitions

bundle exec bin/veewee vbox build CentOS-6.4-x86_64-minimal
bundle exec bin/veewee vbox export CentOS-6.4-x86_64-minimal
# bundle exec vendor/bundle_bin/veewee vbox build CentOS-6.4-x86_64-minimal
# bundle exec vendor/bundle_bin/veewee vbox export CentOS-6.4-x86_64-minimal
```

# [license]
* nothing

[link1]: https://github.com/jedi4ever/veewee
